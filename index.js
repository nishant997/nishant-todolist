let editList = document.getElementsByClassName('edit_button')
for (let i = 0; i < editList.length; i++) {
	editList[i].addEventListener('click', (event) => {
		let previousText = event.target.previousElementSibling.textContent
		let itemName = window.prompt('Please Edit list Items:', previousText)
		if (itemName == null || itemName == '') {
			event.target.previousElementSibling.innerHTML = previousText
		} else {
			event.target.previousElementSibling.innerHTML = itemName
		}
	})
}

let toDoList = document.getElementsByTagName('li')

for (let i = 0; i < toDoList.length; i++) {
	let span = document.createElement('SPAN')
	let txt = document.createTextNode('\u00D7')
	span.className = 'close'
	span.appendChild(txt)
	toDoList[i].appendChild(span)
}

let close = document.getElementsByClassName('close')
for (let i = 0; i < close.length; i++) {
	close[i].onclick = function () {
		let div = this.parentElement
		div.style.display = 'none'
	}
}

// let list = document.querySelector('ul')
// list.addEventListener(
// 	'click',
// 	function (ev) {
// 		if (ev.target.tagName == 'LI' || ev.target.tagName == 'P') {
// 			ev.target.classList.toggle('checked')
// 		}
// 	},
// 	false
// )

for (let i = 0; i < toDoList.length; i++) {
	toDoList[i].addEventListener(
		'click',
		function (ev) {
			ev.target.classList.toggle('checked')
		},
		false
	)
}

let pText = document.getElementsByClassName('input-text')

for (let i = 0; i < pText.length; i++) {
	pText[i].addEventListener(
		'click',
		function (ev) {
			ev.target.parentElement.classList.toggle('checked')
		},
		false
	)
}

function addNewElement() {
	let p = document.createElement('p')
	p.className = 'input-text'
	let editButton = document.createElement('button')
	editButton.className = 'edit_button'
	let editText = document.createTextNode('Edit')
	editButton.append(editText)
	let li = document.createElement('li')
	let inputValue = document.getElementById('toDoInput').value
	let t = document.createTextNode(inputValue)
	p.appendChild(t)
	li.appendChild(p)
	li.appendChild(editButton)
	if (inputValue === '') {
		alert('You did not Enter any Task')
	} else {
		document.getElementById('allList').appendChild(li)
	}
	document.getElementById('toDoInput').value = ''

	let span = document.createElement('span')
	let txt = document.createTextNode('\u00D7')
	span.className = 'close'
	span.appendChild(txt)
	li.appendChild(span)
	for (i = 0; i < close.length; i++) {
		close[i].onclick = function () {
			var div = this.parentElement
			div.style.display = 'none'
		}
	}
	for (let i = 0; i < editList.length; i++) {
		editList[i].addEventListener('click', (event) => {
			let previousText = event.target.previousElementSibling.textContent
			var itemName = prompt('Please Edit list Items:', previousText)
			if (itemName == null || itemName == '') {
				event.target.previousElementSibling.innerHTML = previousText
			} else {
				event.target.previousElementSibling.innerHTML = itemName
			}
		})
	}
}

let filter = document.getElementById('filter')
filter.addEventListener('click', (event) => {
	for (let i = 0; i < toDoList.length; i++) {
		switch (event.target.value) {
			case 'all':
				toDoList[i].style.display = 'flex'
				break
			case 'completed':
				toDoList[i].classList.contains('checked')
					? (toDoList[i].style.display = 'flex')
					: (toDoList[i].style.display = 'none')
				break
			case 'pending':
				!toDoList[i].classList.contains('checked')
					? (toDoList[i].style.display = 'flex')
					: (toDoList[i].style.display = 'none')
				break
		}
	}
})
